package edu.ib.springdata.manager;

import edu.ib.springdata.model.customer.Customer;
import edu.ib.springdata.model.customer.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Optional;

@Service
public class CustomerService {

    private CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public Iterable<Customer> findAll() {
        return customerRepository.findAll();
    }

    public Optional<Customer> findById(Long id) {
        return customerRepository.findById(id);
    }

    public Customer addCustomer(Customer customer) {
        return customerRepository.save(customer);
    }

    public Customer updateCustomer(Long id, Customer customer) throws ChangeSetPersister.NotFoundException {
        Customer customerFromDatabase = customerRepository.findById(id).orElseThrow(ChangeSetPersister.NotFoundException::new);
        customerFromDatabase.setName(customer.getName());
        customerFromDatabase.setAdress(customer.getAdress());
        return customerRepository.save(customerFromDatabase);
    }

    public Customer modifyCustomer(Long id, Customer customer) throws ChangeSetPersister.NotFoundException {
        Customer customerFromDatabase = customerRepository.findById(id).orElseThrow(ChangeSetPersister.NotFoundException::new);


        if (StringUtils.hasLength(customer.getName())) {
            customerFromDatabase.setName(customer.getName());
        }

        if (StringUtils.hasLength(customer.getAdress())) {
            customerFromDatabase.setAdress(customer.getAdress());
        }


        return customerRepository.save(customerFromDatabase);
    }

}
