package edu.ib.springdata.manager;

import edu.ib.springdata.model.order.Order;
import edu.ib.springdata.model.order.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Optional;

@Service
public class OrderService {

    private OrderRepository orderRepository;

    @Autowired
    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }


    public Iterable<Order> findAll() {
        return orderRepository.findAll();
    }

    public Optional<Order> findById(Long id) {
        return orderRepository.findById(id);
    }

    public Order addOrder(Order order) {
        return orderRepository.save(order);
    }

    public Order updateOrder(Long id, Order order) throws ChangeSetPersister.NotFoundException {
        Order orderFromDatabase = orderRepository.findById(id).orElseThrow(ChangeSetPersister.NotFoundException::new);
        orderFromDatabase.setCustomer(order.getCustomer());
        orderFromDatabase.setPlaceDate(order.getPlaceDate());
        orderFromDatabase.setProducts(order.getProducts());
        orderFromDatabase.setStatus(order.getStatus());

        return orderRepository.save(orderFromDatabase);
    }

    public Order modifyOrder(Long id, Order order) throws ChangeSetPersister.NotFoundException {
        Order orderFromDatabase = orderRepository.findById(id).orElseThrow(ChangeSetPersister.NotFoundException::new);

        if (StringUtils.hasLength(order.getStatus())) {
            orderFromDatabase.setStatus(order.getStatus());
        }

        if (order.getCustomer() != null) {
            orderFromDatabase.setCustomer(order.getCustomer());
        }

        if (order.getProducts() != null) {
            orderFromDatabase.setProducts(order.getProducts());
        }

        if (order.getPlaceDate() != null) {
            orderFromDatabase.setPlaceDate(order.getPlaceDate());
        }
        
        return orderRepository.save(orderFromDatabase);

    }
}
