package edu.ib.springdata.manager;

import edu.ib.springdata.model.product.Product;
import edu.ib.springdata.model.product.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Optional;

@Service
public class ProductService {

    private ProductRepository productRepo;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepo = productRepository;
    }

    public Iterable<Product> findAll() {
        return productRepo.findAll();
    }

    public Optional<Product> findById(Long id) {
        return productRepo.findById(id);
    }

    public Product addProduct(Product product) {
        return productRepo.save(product);
    }

    public Product updateProduct(Long id, Product product) throws ChangeSetPersister.NotFoundException {
        Product productFromDatabase = productRepo.findById(id).orElseThrow(ChangeSetPersister.NotFoundException::new);
        productFromDatabase.setName(product.getName());
        productFromDatabase.setPrice(product.getPrice());
        productFromDatabase.setAvailable(product.isAvailable());
        return productRepo.save(productFromDatabase);
    }

    public Product modifyProduct(Long id, Product product) throws ChangeSetPersister.NotFoundException {
        Product productFromDatabase = productRepo.findById(id).orElseThrow(ChangeSetPersister.NotFoundException::new);

        if (StringUtils.hasLength(product.getName())) {
            productFromDatabase.setName(product.getName());
        }

        if (product.getPrice() > 0) {
            productFromDatabase.setPrice(product.getPrice());
        }

        if(String.valueOf(product.isAvailable()).startsWith("f")){
            productFromDatabase.setAvailable(false);
        }else{
            productFromDatabase.setAvailable(true);
        }

        productRepo.save(productFromDatabase);


        return productFromDatabase;


    }

}
