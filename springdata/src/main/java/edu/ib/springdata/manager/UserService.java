package edu.ib.springdata.manager;

import edu.ib.springdata.model.user.UserDto;
import edu.ib.springdata.model.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserDto findByName(String name) {
        Iterable<UserDto> users = userRepository.findAll();
        UserDto user = null;
        for (UserDto u : users) {
            if (u.getName().equals(name)) {
                user = u;
                break;
            }
        }
        return user;
    }

    public UserDto addUser(UserDto userDto) {
        return userRepository.save(userDto);
    }
}
