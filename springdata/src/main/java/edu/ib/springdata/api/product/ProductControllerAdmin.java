package edu.ib.springdata.api.product;

import edu.ib.springdata.manager.ProductService;
import edu.ib.springdata.model.product.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/admin/product")
public class ProductControllerAdmin {

    private ProductService productService;

    @Autowired
    public ProductControllerAdmin(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping
    public Product addProuct(@RequestBody Product product) {
        return productService.addProduct(product);
    }

    @PutMapping
    public Product updateProduct(@RequestParam Long id, @RequestBody Product product) throws ChangeSetPersister.NotFoundException {
        return productService.updateProduct(id, product);
    }

    @PatchMapping
    public Product modifyProduct(@RequestParam Long id, @RequestBody Product product) throws ChangeSetPersister.NotFoundException {
        return productService.modifyProduct(id, product);
    }
}
