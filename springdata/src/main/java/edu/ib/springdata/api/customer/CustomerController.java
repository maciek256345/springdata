package edu.ib.springdata.api.customer;

import edu.ib.springdata.manager.CustomerService;
import edu.ib.springdata.model.customer.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/customer")
public class CustomerController {

    private CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/all")
    public Iterable<Customer> getAll() {
        return customerService.findAll();
    }

    @GetMapping
    public Optional<Customer> getById(@RequestParam Long id) {
        return customerService.findById(id);
    }


}

