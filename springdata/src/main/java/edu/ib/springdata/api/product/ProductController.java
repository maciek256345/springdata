package edu.ib.springdata.api.product;

import edu.ib.springdata.manager.ProductService;
import edu.ib.springdata.model.product.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/api/product")
public class ProductController {

    private ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/all")
    public Iterable<Product> getAll() {
        return productService.findAll();
    }

    @GetMapping
    public Optional<Product> getById(@RequestParam Long id) {
        return productService.findById(id);
    }
}
