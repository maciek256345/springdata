package edu.ib.springdata.api.customer;

import edu.ib.springdata.manager.CustomerService;
import edu.ib.springdata.model.customer.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/admin/customer")
public class CustomerControllerAdmin {

    private CustomerService customerService;

    @Autowired
    public CustomerControllerAdmin(CustomerService customerService) {
        this.customerService = customerService;
    }

    @PostMapping
    public Customer addCustomer(@RequestBody Customer customer) {
        return customerService.addCustomer(customer);
    }

    @PutMapping
    public Customer updateCustomer(@RequestParam Long id, @RequestBody Customer customer) throws ChangeSetPersister.NotFoundException {
        return customerService.updateCustomer(id, customer);
    }

    @PatchMapping
    public Customer modifyCustomer(@RequestParam Long id, @RequestBody Customer customer) throws ChangeSetPersister.NotFoundException {
        return customerService.modifyCustomer(id, customer);
    }


}
