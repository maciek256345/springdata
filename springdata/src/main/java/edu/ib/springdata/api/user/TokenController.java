package edu.ib.springdata.api.user;

import edu.ib.springdata.manager.UserService;
import edu.ib.springdata.model.user.User;
import edu.ib.springdata.model.user.UserDto;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;

import java.util.Date;

@RestController
public class TokenController {

    UserService userService;
    PasswordEncoder passwordEncoder;

    @Autowired
    public TokenController(UserService userService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping("/api/getToken")
    public String getToken(@RequestBody User user) throws Exception {
        UserDto userDto = userService.findByName(user.getName());

        if (!passwordEncoder.matches(user.getPassword(), userDto.getPasswordHash())) {
            throw new HttpServerErrorException(HttpStatus.FORBIDDEN);
        }

        long time = System.currentTimeMillis();
        return Jwts.builder().setSubject(user.getName())
                .claim("role", userDto.getRole())
                .setIssuedAt(new Date(time))
                .setExpiration(new Date(time + 3600000))
                .signWith(SignatureAlgorithm.HS512, user.getPassword())
                .compact();

    }
}
