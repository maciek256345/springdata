package edu.ib.springdata.api.order;

import edu.ib.springdata.manager.OrderService;
import edu.ib.springdata.model.order.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/order")
public class OrderController {

    private OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("/all")
    public Iterable<Order> getAll() {
        return orderService.findAll();
    }

    @GetMapping
    public Optional<Order> getById(@RequestParam Long id) {
        return orderService.findById(id);
    }

    @PostMapping
    public Order addOrder(@RequestBody Order order) {
        return orderService.addOrder(order);
    }

}
