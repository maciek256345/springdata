package edu.ib.springdata.api.order;

import edu.ib.springdata.manager.OrderService;
import edu.ib.springdata.model.order.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/admin/order")
public class OrderControllerAdmin {

    private OrderService orderService;

    @Autowired
    public OrderControllerAdmin(OrderService orderService) {
        this.orderService = orderService;
    }

    @PutMapping
    public Order updateOrder(@RequestParam Long id, @RequestBody Order order) throws ChangeSetPersister.NotFoundException {
        return orderService.updateOrder(id, order);
    }

    @PatchMapping
    public Order modifyOrder(@RequestParam Long id, @RequestBody Order order) throws ChangeSetPersister.NotFoundException {
        return orderService.modifyOrder(id, order);
    }

}
