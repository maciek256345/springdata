package edu.ib.springdata.model.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserDtoBuilder {

    PasswordEncoder passwordEncoder;

    @Autowired
    public UserDtoBuilder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }


    public UserDto buildUserDto(User user) {
        String password = user.getPassword();
        String hashPassword = passwordEncoder.encode(password);

        return new UserDto(user.getName(), hashPassword, user.getRole());
    }
}
