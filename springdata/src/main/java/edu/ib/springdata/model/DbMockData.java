package edu.ib.springdata.model;

import edu.ib.springdata.model.customer.Customer;
import edu.ib.springdata.model.customer.CustomerRepository;
import edu.ib.springdata.model.order.Order;
import edu.ib.springdata.model.order.OrderRepository;
import edu.ib.springdata.model.product.Product;
import edu.ib.springdata.model.product.ProductRepository;
import edu.ib.springdata.model.user.User;
import edu.ib.springdata.model.user.UserDto;
import edu.ib.springdata.model.user.UserDtoBuilder;
import edu.ib.springdata.model.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Component
public class DbMockData {
    private ProductRepository productRepository;
    private OrderRepository orderRepository;
    private CustomerRepository customerRepository;
    private UserDtoBuilder userDtoBuilder;
    private UserRepository userRepository;

    @Autowired
    public DbMockData(ProductRepository productRepository, OrderRepository orderRepository, CustomerRepository customerRepository, UserDtoBuilder userDtoBuilder, UserRepository userRepository) {
        this.productRepository = productRepository;
        this.orderRepository = orderRepository;
        this.customerRepository = customerRepository;
        this.userDtoBuilder = userDtoBuilder;
        this.userRepository = userRepository;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fill() {
        Product product1 = new Product("Doniczka", 25.90, true);
        Product product2 = new Product("Ziema ogrodowa 25L", 45, true);
        Product product3 = new Product("Nawóz uniwersalny 3L", 25.55, true);

        Customer customer1 = new Customer("Kamil Kowalski", "Wrocław");
        Customer customer2 = new Customer("Jan Szybki", "Kraków");
        Customer customer3 = new Customer("Krystyna Mięta", "Nowy Targ");

        UserDto user1 = userDtoBuilder.buildUserDto(new User("Maciej", "tajnehaslo2022", "ROLE_ADMIN"));
        UserDto user2 = userDtoBuilder.buildUserDto(new User("Helena", "tajnehaslo2022", "ROLE_CUSTOMER"));

        Set<Product> products1 = new HashSet() {
            {
                add(product1);
                add(product2);

            }
        };
        Set<Product> products2 = new HashSet() {
            {
                add(product1);
                add(product2);
                add(product3);

            }
        };

        Order order1 = new Order(customer1, products1, LocalDateTime.now(), "to pay");
        Order order2 = new Order(customer2, products2, LocalDateTime.now(), "paid");

        productRepository.save(product1);
        productRepository.save(product2);
        productRepository.save(product3);

        customerRepository.save(customer1);
        customerRepository.save(customer2);
        customerRepository.save(customer3);

        orderRepository.save(order1);
        orderRepository.save(order2);

        userRepository.save(user1);
        userRepository.save(user2);

    }
}
