CUSTOMER

GET http://localhost:8008/api/customer/all
GET http://localhost:8008/api/customer?id=2
POST http://localhost:8008/api/admin/customer
{ 
    "name": "Wanda Krysiak",
    "adress": "Gdańsk"
}

PUT http://localhost:8008/api/admin/customer?id=1
{
    "name": "Kamil Kotowski",
    "adress": "Warszawa"
}

PATCH http://localhost:8008/api/admin/customer?id=2
{
    "adress": "Sosnowiec"
}
------------------------------------------------------------------
PRODUCT

GET http://localhost:8008/api/product/all
GET http://localhost:8008/api/product?id=3
POST http://localhost:8008/api/admin/product
{
    "name": "Taczka",
    "price": 155,
    "available": true
}
PUT http://localhost:8008/api/admin/product?id=1
{
    "name": "Kora ogrodowa",
    "price": 85.50,
    "available": true
}

PATCH http://localhost:8008/api/admin/product?id=3
{
    "available": false
}
------------------------------------------------------------------
ORDER

GET http://localhost:8008/api/order/all
GET http://localhost:8008/api/order?id=2
POST http://localhost:8008/api/order
 {
        "customer": {
            "id": 1,
            "name": "Kamil Kowalski",
            "adress": "Wrocław"
        },
        "products": [
             {
                "id": 3,
                "name": "Nawóz uniwersalny 3L",
                "price": 25.55,
                "available": false
            }
        ],
        "placeDate": "2022-05-02T19:48:32",
        "status": "to pay"
}

PUT http://localhost:8008/api/admin/order?id=1
{
        "customer": {
            "id": 1,
            "name": "Kamil Kowalski",
            "adress": "Wrocław"
        },
        "products": [
            {
                "id": 1,
                "name": "Doniczka",
                "price": 25.9,
                "available": true
            },
            {
                "id": 3,
                "name": "Nawóz uniwersalny 3L",
                "price": 25.55,
                "available": true
            }
           
        ],
        "placeDate": "2022-04-02T19:51:55",
        "status": "to pay"
}
PATCH http://localhost:8008/api/admin/order?id=2
{
       "status": "to pay"
}